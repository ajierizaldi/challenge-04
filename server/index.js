// console.log("Implement servermu disini yak 😝!");
require('dotenv').config();
const PORT = process.env.PORT || 8080;

const HttpServer = require('http');
const FS = require('fs');
const PATH = require('path');
const PUBLIC_DIR = PATH.join(__dirname, '..', 'public');

class server {
    constructor(port, ip) {
        this.#createServer().listen(port, ip, () => console.log(`Server berhasil dijalankan di ${port}`)) 
    }

    #createServer() {
        return HttpServer.createServer((req, res) => this.#route(req, res))
    }

    #route(req, res) {
        if (req.url === '/') {
            res.setHeader('Content-Type', 'text/html');
            res.writeHead(200);
            res.end(this.#getHTML('/landingPages.html'))
        } else if (req.url === '/cariMobil') {
            res.setHeader('Content-Type', 'text/html');
            res.writeHead(200);
            res.end(this.#getHTML('/cariMobil.html'))
        } else if (req.url === '/indexExample') {
            res.setHeader('Content-Type', 'text/html');
            res.writeHead(200);
            res.end(this.#getHTML('/index.example.html'))
        } else if (req.url.match("\.css$")) {
            let filePath = PATH.join(__dirname, '..', 'public', req.url);
            let fileStream = FS.createReadStream(filePath, 'utf-8');
            res.writeHead(200, { 'Content-Type': 'text/css' });
            fileStream.pipe(res);
        } else if (req.url.match("\.js$")) {
            let filePath = PATH.join(__dirname, '..', 'public', req.url);
            let fileStream = FS.createReadStream(filePath, 'utf-8');
            res.writeHead(200, { 'Content-Type': 'text/javascript' });
            fileStream.pipe(res);
        } else if (req.url.match(".ttf$")) {
                let filePath = PATH.join(PUBLIC_DIR, req.url);
                let fileStream = FS.createReadStream(filePath, "UTF-8");
                fileStream.pipe(res);
            } else if (
                req.url.match(".jpg$") ||
                req.url.match(".png$") ||
                req.url.match(".jpeg$") ||
                req.url.match(".svg$")
              ) {
                let filePath = PATH.join(PUBLIC_DIR, req.url);
                const fileType = filePath.split(".")[1];
                res.writeHead(200, {
                  "Content-Type": `${
                    fileType === "svg" ? "image/svg+xml" : `image/${fileType}`
                  }`,
                });
                FS.readFile(filePath, function (err, content) {
                  res.end(content);
                });
        } else {
            res.setHeader('Content-Type', 'text/html');
            res.writeHead(200);
            res.end('404')
        }
    }

    #getHTML(htmlFileName) {
        const fileHtml = PATH.join(PUBLIC_DIR, htmlFileName);
        return FS.readFileSync(fileHtml, 'utf-8');
    }
}

const serverBaru = new server(PORT, '0.0.0.0');

